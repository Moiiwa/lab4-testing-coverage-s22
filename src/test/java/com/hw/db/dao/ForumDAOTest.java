package com.hw.db.dao;

import controllers.db.DAO.ForumDAO;
import controllers.db.DAO.UserDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDAOTest {


    @Test
    void getUserListWithoutSinceAndDescendingAndLimit(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new ForumDAO(jdbcMock);
        ForumDAO.UserList("", null, null, null);
        verify(jdbcMock).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.eq(new Object[] { "" }), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void getUserListWithoutDescendingAndLimit(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new ForumDAO(jdbcMock);
        ForumDAO.UserList("", null, "3", null);
        verify(jdbcMock).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.eq(new Object[] { "", "3" }), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void getUserListWithoutLimit(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new ForumDAO(jdbcMock);
        ForumDAO.UserList("", null, "3", true);
        verify(jdbcMock).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Mockito.eq(new Object[] { "", "3" }), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void getUserListWithAllData(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new ForumDAO(jdbcMock);
        ForumDAO.UserList("", 3, "3", true);
        verify(jdbcMock).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.eq(new Object[] { "", "3", 3 }), Mockito.any(UserDAO.UserMapper.class));
    }




}
