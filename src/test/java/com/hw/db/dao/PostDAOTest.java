package com.hw.db.dao;

import controllers.db.DAO.PostDAO;
import controllers.db.models.Post;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PostDAOTest {

    @Test
    void setPostWithoutAuthorAndMessage(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        Post post = new Post();
        post.setCreated(Timestamp.valueOf(LocalDateTime.now()));
        Post post1 = new Post();
        post1.setCreated(Timestamp.valueOf(LocalDateTime.now().minusDays(1)));
        when(jdbcMock.queryForObject
                (anyString(),any(RowMapper.class),any()))
                .thenReturn(post1);
        new PostDAO(jdbcMock);
        PostDAO.setPost(1, post);
        verify(jdbcMock).update(eq(
                "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                eq(post.getCreated()), eq(1));
    }

    @Test
    void setPostWithoutMessage(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        Post post = new Post();
        post.setCreated(Timestamp.valueOf(LocalDateTime.now()));
        post.setAuthor("aboba");
        Post post1 = new Post();
        post1.setAuthor("abiba");
        post1.setCreated(Timestamp.valueOf(LocalDateTime.now().minusDays(1)));
        when(jdbcMock.queryForObject
                (anyString(),any(RowMapper.class),any()))
                .thenReturn(post1);
        new PostDAO(jdbcMock);
        PostDAO.setPost(1, post);
        verify(jdbcMock).update(eq(
                "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                eq(post.getAuthor()),eq(post.getCreated()), eq(1));
    }

    @Test
    void setPostWithoutAuthorAndCreated(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        Post post = new Post();
        post.setMessage("aboba");
        Post post1 = new Post();
        post1.setMessage("abiba");
        when(jdbcMock.queryForObject
                (anyString(),any(RowMapper.class),any()))
                .thenReturn(post1);
        new PostDAO(jdbcMock);
        PostDAO.setPost(1, post);
        verify(jdbcMock).update(eq(
                "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                eq(post.getMessage()), eq(1));
    }

    @Test
    void setPostWithoutCreated(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        Post post = new Post();
        post.setAuthor("boba");
        post.setMessage("aboba");
        Post post1 = new Post();
        post1.setAuthor("biba");
        post1.setMessage("abiba");
        when(jdbcMock.queryForObject
                (anyString(),any(RowMapper.class),any()))
                .thenReturn(post1);
        new PostDAO(jdbcMock);
        PostDAO.setPost(1, post);
        verify(jdbcMock).update(eq(
                "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                eq(post.getAuthor()),eq(post.getMessage()), eq(1));
    }

}
