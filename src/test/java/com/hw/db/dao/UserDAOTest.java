package com.hw.db.dao;

import controllers.db.DAO.UserDAO;
import controllers.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

public class UserDAOTest {

    @Test
    void changeWithoutEmailAndFullNameAndAbout(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();

        UserDAO.Change(user);
        verifyNoInteractions(jdbcMock);
    }

    @Test
    void changeWithoutFullNameAndAbout(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();
        user.setEmail("aboba");
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(
                "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("aboba"), Mockito.eq(null));
    }

    @Test
    void changeWithoutAbout(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();
        user.setEmail("aboba");
        user.setFullname("abiba");
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(
                "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("aboba"), Mockito.eq("abiba"), Mockito.eq(null));
    }

    @Test
    void changeWithAll(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();
        user.setEmail("aboba");
        user.setFullname("abiba");
        user.setAbout("abeba");
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(
                "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("aboba"), Mockito.eq("abiba"), Mockito.eq("abeba"), Mockito.eq(null));
    }

    @Test
    void changeWithoutEmailAndAbout(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();
        user.setFullname("abiba");
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(
                "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("abiba"), Mockito.eq(null));
    }

    @Test
    void changeWithoutEmailAndFullName(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();
        user.setAbout("abeba");
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(
                "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("abeba"), Mockito.eq(null));
    }

    @Test
    void changeWithoutFullName(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();
        user.setEmail("aboba");
        user.setAbout("abeba");
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(
                "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("aboba"), Mockito.eq("abeba"), Mockito.eq(null));
    }

    @Test
    void changeWithoutEmail(){
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
        User user = new User();
        user.setFullname("abiba");
        user.setAbout("abeba");
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(
                "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("abiba"), Mockito.eq("abeba"), Mockito.eq(null));
    }


}
