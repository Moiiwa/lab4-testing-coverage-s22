package com.hw.db.dao;

import controllers.db.DAO.PostDAO;
import controllers.db.DAO.ThreadDAO;
import controllers.db.DAO.UserDAO;
import controllers.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTest {

    @Test
    void treeSortDescending(){
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new ThreadDAO(jdbcMock);
        ThreadDAO.treeSort(1,1,1,true);
        verify(jdbcMock).query(Mockito.eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                any(PostDAO.PostMapper.class), Mockito.eq(1), Mockito.eq(1), Mockito.eq(1));
    }

    @Test
    void treeSortAscending(){
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        JdbcTemplate jdbcMock = mock(JdbcTemplate.class);
        new ThreadDAO(jdbcMock);
        ThreadDAO.treeSort(1,1,1,true);
        verify(jdbcMock).query(Mockito.eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                any(PostDAO.PostMapper.class), Mockito.eq(1), Mockito.eq(1), Mockito.eq(1));
    }
}
