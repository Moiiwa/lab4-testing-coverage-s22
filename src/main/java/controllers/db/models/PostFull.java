package controllers.db.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PostFull {
    private controllers.db.models.User User;
    private controllers.db.models.Forum Forum;
    private controllers.db.models.Post Post;
    private controllers.db.models.Thread Thread;
    @JsonCreator
    PostFull(
            @JsonProperty("user") controllers.db.models.User User,
            @JsonProperty("forum") controllers.db.models.Forum Forum,
            @JsonProperty("post") controllers.db.models.Post Post,
            @JsonProperty("thread") controllers.db.models.Thread Thread
    ){
        this.User=User;
        this.Forum=Forum;
        this.Post=Post;
        this.Thread=Thread;
    }

    public controllers.db.models.User getUser() {
        return User;
    }

    public void setUser(controllers.db.models.User user) {
        User = user;
    }

    public controllers.db.models.Forum getForum() {
        return Forum;
    }

    public void setForum(controllers.db.models.Forum forum) {
        Forum = forum;
    }

    public controllers.db.models.Post getPost() {
        return Post;
    }

    public void setPost(controllers.db.models.Post post) {
        Post = post;
    }

    public controllers.db.models.Thread getThread() {
        return Thread;
    }

    public void setThread(controllers.db.models.Thread thread) {
        Thread = thread;
    }
}
